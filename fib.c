#include <stdio.h>

int main() {
	int n = 5;
	int fib1 = 0;
	int fib2 = 1;
	int currentfib;
	int i = 2;
	printf("0\n1\n");
	for (i; i < n; i++) {
		currentfib = fib1 + fib2;
		fib1 = fib2;
		fib2 = currentfib;
		printf("%d\n",currentfib);
	}
}
