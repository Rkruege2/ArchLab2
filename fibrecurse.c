#include <stdio.h>

int recurse(int n, int fib1, int fib2) {

	if (n == 0) {
		return 0;
	}
	int currentfib = fib1 + fib2;
	printf("%d\n", currentfib);
	recurse(n-1,fib2,currentfib);
	return 0;

}

int main() {
	int n = 5;
	printf("0\n1\n");
	recurse(n-2,0,1);

}
